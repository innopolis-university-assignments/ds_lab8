from multiprocessing import Process, Pipe
from functools import partial
from time import sleep


def send_message(pipe, p_name, clocs):
    clocs[p_name] += 1
    pipe.send(clocs)
    return clocs


def recv_message(pipe, p_name, clocs):
    clocs[p_name] += 1
    rec_clocs = pipe.recv()
    for p in clocs:
        clocs[p] = max(clocs[p], rec_clocs[p])
    return clocs

def event(p_name, clocs):
    clocs[p_name] += 1
    return clocs


def run_process(p_name, clocs, actions):
    for action in actions:
        clocs = action(clocs)
        sleep(0.01)
    print(f"Process {p_name}: {list(clocs.values())}")


if __name__ == "__main__":
    pipe_a_b, pipe_b_a = Pipe()
    pipe_b_c, pipe_c_b = Pipe()
    clocs = {"a": 0, "b": 0, "c": 0}
    processes = []

    processes.append(Process(target=run_process, args=("a", clocs, (
        partial(send_message, pipe_a_b, "a"),
        partial(send_message, pipe_a_b, "a"),
        partial(event, "a"),
        partial(recv_message, pipe_a_b, "a"),
        partial(event, "a"),
        partial(event, "a"),
        partial(recv_message, pipe_a_b, "a"),
    ))))
    processes.append(Process(target=run_process, args=("b", clocs, (
        partial(recv_message, pipe_b_a, "b"),
        partial(recv_message, pipe_b_a, "b"),
        partial(send_message, pipe_b_a, "b"),
        partial(recv_message, pipe_b_c, "b"),
        partial(event, "b"),
        partial(send_message, pipe_b_a, "b"),
        partial(send_message, pipe_b_c, "b"),
        partial(send_message, pipe_b_c, "b"),
    ))))
    processes.append(Process(target=run_process, args=("c", clocs, (
        partial(send_message, pipe_c_b, "c"),
        partial(recv_message, pipe_c_b, "c"),
        partial(event, "c"),
        partial(recv_message, pipe_c_b, "c"),
    ))))

    print(processes)
    for process in processes:
        process.start()


    for process in processes:
        process.join()